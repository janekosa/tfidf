import java.util.Map;
import java.util.Objects;

/**
 * Created by janekosa on 21.01.2018.
 */
public class TokenizedDocument {

    private String originalText;
    private Integer wordCount;
    private Map<String, Double> tokensTF;

    public String getOriginalText() {
        return originalText;
    }

    public void setOriginalText(String originalText) {
        this.originalText = originalText;
    }

    public Integer getWordCount() {
        return wordCount;
    }

    public void setWordCount(Integer wordCount) {
        this.wordCount = wordCount;
    }

    public Map<String, Double> getTokensTF() {
        return tokensTF;
    }

    public void setTokensTF(Map<String, Double> tokensTF) {
        this.tokensTF = tokensTF;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TokenizedDocument document = (TokenizedDocument) o;
        return Objects.equals(originalText, document.originalText);
    }

    @Override
    public int hashCode() {
        return Objects.hash(originalText);
    }
}
