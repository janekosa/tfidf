import java.util.*;
import java.util.stream.Collectors;

/**
 * Created by janekosa on 21.01.2018.
 */
public class Indexer {

    private Tokenizer tokenizer = Tokenizer.getInstance();

    private List<TokenizedDocument> documents = new LinkedList<>();

    private Map<String, Set<TokenizedDocument>> documentsWithTokens;

    private Map<String, Double> idfs;


    public void addDocument(String document) {
        documents.add(tokenizer.tokenize(document));
    }

    public void removeDocument(String document) {
        documents.remove(tokenizer.tokenize(document));
    }

    public void index() {
        documentsWithTokens = new HashMap<>();
        for (TokenizedDocument document : documents) {
            document.getTokensTF().keySet().forEach((token) -> {
                Set<TokenizedDocument> documentsWithToken = documentsWithTokens.getOrDefault(token, new HashSet<>());
                documentsWithToken.add(document);
                documentsWithTokens.put(token, documentsWithToken);
            });
        }
        idfs = new HashMap<>();
        documentsWithTokens.forEach((token, docs) -> {
            Double idf = Math.log10((double) docs.size() / (double) documents.size());
            idfs.put(token, idf);
        });
    }

    public List<String> search(String searchQuery) {
        if (searchQuery == null) {
            throw new IllegalArgumentException("The search query cannot be null.");
        }
        searchQuery = searchQuery.toLowerCase(); // default locale 
        String[] tokens = searchQuery.split(Tokenizer.DEFAULT_SPLIT_REGEX);
        if (tokens.length == 0) {
            throw new IllegalArgumentException("The search query contains no valid words.");
        }
        Map<String, Double> docVals = new HashMap<>();
        for (String searchToken : tokens) {
            Set<TokenizedDocument> docsForToken = documentsWithTokens.get(searchToken);
            docsForToken.forEach((doc) -> {
                Double tf = (double) doc.getTokensTF().get(searchToken);
                Double addedVal = tf*idfs.get(searchToken);
                docVals.put(doc.getOriginalText(), addedVal + docVals.getOrDefault(doc.getOriginalText(), 0D));
            });
        }

        return docVals.entrySet().stream().sorted(Comparator.comparingDouble(Map.Entry::getValue)).map(Map.Entry::getKey).collect(Collectors.toList());

    }

}
