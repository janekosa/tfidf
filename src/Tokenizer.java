import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

/**
 * Created by janekosa on 21.01.2018.
 */
public class Tokenizer {

    public static final String DEFAULT_SPLIT_REGEX = "\\s+"; // Note that this will not behave correctly if the document contains special characters etc.

    private static Tokenizer instance = new Tokenizer();

    public static Tokenizer getInstance() {
        return instance;
    }

    public TokenizedDocument tokenize(String document) {
        return tokenize(document, DEFAULT_SPLIT_REGEX);
    }

    public TokenizedDocument tokenize(String document, String splittingRegex) {
        if (document == null || document.isEmpty()) {
            throw new IllegalArgumentException("Document to tokenize cannot be null or empty.");
        }
        document = document.toLowerCase(); // Note that this will use default locale. For simplicity of solution I ignore this issue.
        String[] words = document.split(splittingRegex);
        if (words.length == 0) {
            throw new IllegalArgumentException("Document to tokenize contains no correct words.");
        }
        Map<String, Double> tokensTF = new HashMap<>();
        for (String word : words) {
            Double termFrequency = tokensTF.getOrDefault(word, 0D);
            termFrequency += 1/(double) words.length;
            tokensTF.put(word, termFrequency);
        }
        TokenizedDocument res = new TokenizedDocument();
        res.setTokensTF(tokensTF);
        res.setWordCount(words.length);
        res.setOriginalText(document);
        return res;
    }
}
