public class Main {

    public static void main(String[] args) {
        String document1 = "the brown fox jumped over the brown dog";
        String document2 = "the lazy brown dog sat in the corner";
        String document3 = "the red fox bit the lazy dog";

        Indexer indexer = new Indexer();
        indexer.addDocument(document1);
        indexer.addDocument(document2);
        indexer.addDocument(document3);

        indexer.index();
        indexer.search("brown").forEach(System.out::println);

        System.out.println();

        indexer.search("fox").forEach(System.out::println);

        System.out.println();
    }
}
